const { generatePrimes } = require('./primeGenerator');

const min = 2;
const max = 1e7;
const numberOfWorkers = 4;
generatePrimes(min, max, numberOfWorkers).then((primes) => {
  const message = 'Prime is : ' + primes.join(' ');
  console.log(message);
});
