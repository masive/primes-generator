const { Worker, isMainThread } = require('worker_threads');
const path = require('path');

function generatePrimes(min, max, numThreads) {
  return new Promise((resolve, reject) => {
    if (isMainThread) {
      const segmentSize = Math.ceil((max - min + 1) / numThreads);
      const primes = [];

      const workers = [];
      let currentIndex = min;

      const handleWorkerExit = () => {
        if (workers.every((w) => w.finished)) {
          const sortedPrimes = primes.flat().sort((a, b) => a - b);
          resolve(sortedPrimes);
        }
      };

      for (let i = 0; i < numThreads; i++) {
        const start = currentIndex;
        const end = Math.min(currentIndex + segmentSize - 1, max);
        currentIndex = end + 1;
        const worker = new Worker(path.join(__dirname, 'primeWorker.js'), {
          workerData: { start, end },
        });

        worker.on('message', (message) => {
          primes.push(message);
        });

        worker.finished = false;
        worker.once('exit', () => {
          worker.finished = true;
          handleWorkerExit();
        });

        workers.push(worker);
      }
    } else {
      reject(new Error('generatePrimesInRange should be called in the main thread'));
    }
  });
}

module.exports = {
  generatePrimes,
};
