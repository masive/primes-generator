const { generatePrimes } = require('./primeGenerator');

describe('Prime number generation', () => {
  test('Generates prime numbers within range 2 to 20', async () => {
    const min = 2;
    const max = 20;
    const numberOfWorkers = 2;
    const expectedPrimes = [2, 3, 5, 7, 11, 13, 17, 19];

    const primes = await generatePrimes(min, max, numberOfWorkers);
    expect(primes).toEqual(expectedPrimes);
  });
});
