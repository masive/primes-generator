const { parentPort, workerData } = require('worker_threads');

const min = 2;

function checkPrime(start, end) {
  const primes = [];
  for (let i = start; i <= end; i++) {
    let isPrime = true;
    for (let j = min; j <= Math.sqrt(i); j++) {
      if (i !== j && i % j === 0) {
        isPrime = false;
        break;
      }
    }
    if (isPrime && i > 1) {
      primes.push(i);
    }
  }
  parentPort.postMessage(primes);
}

const { start, end } = workerData;
checkPrime(start, end);
parentPort.close();
